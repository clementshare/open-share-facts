function showSubmitErrorMessage(show) {
  showElement(show, "add-product__missing-server-error");
}

function showMissingFieldErrorMessage(show) {
  showElement(show, "add-product__missing-field-error");
}

function showElement(show, id) {
  let element = document.getElementById(id);
  if (show) {
    element.classList.remove("hide");
  } else {
    element.classList.add("hide");
  }
}

function configureInputs() {
  const inputElements = document.getElementsByClassName(
    "add-product__form-input-js",
  );
  Array.from(inputElements).forEach((inputElement) => {
    inputElement.addEventListener("focus", () => {
      showMissingFieldErrorMessage(false);
      showSubmitErrorMessage(false);
    });
  });
}

function configureForm() {
  const addProductForm = document.getElementById("add-product-form");
  addProductForm.addEventListener("submit", (event) => {
    event.preventDefault();
    const name = document.getElementById("name").value;
    const carbonFootprint =
      document.getElementById("carbon-footprint").value * 1;
    const totalUsageDuration =
      document.getElementById("total-usage-duration").value * 1;
    const yearlyUsageDuration =
      document.getElementById("yearly-usage-duration").value * 1;
    const lifespan = document.getElementById("lifespan").value * 1;

    const sourceElements = document.getElementsByClassName(
      "add-product__source-js",
    );
    const sources = Array.from(sourceElements)
      .map((sourceElement) => sourceElement.value)
      .filter((source) => source);

    if (
      name &&
      carbonFootprint &&
      ((lifespan && yearlyUsageDuration) || totalUsageDuration) &&
      sources.length
    ) {
      fetch(addProductForm.action, {
        method: addProductForm.method,
        headers: new Headers({
          "Content-Type": "application/json",
        }),
        body: JSON.stringify({
          name,
          carbonFootprint,
          usage: {
            ...(yearlyUsageDuration && { yearlyUsage: yearlyUsageDuration }),
            ...(lifespan && { lifespan }),
            ...(totalUsageDuration && { totalUsage: totalUsageDuration }),
          },
          sources,
        }),
      }).then((response) => {
        // Clear the form
        if (response.status >= 200 && response.status < 400) {
          window.location.reload();
        } else {
          showSubmitErrorMessage(true);
        }
      });
    } else {
      showMissingFieldErrorMessage(true);
    }
  });
}

function configureAddSource() {
  document
    .getElementById("add-product__add-source")
    .addEventListener("click", (event) => {
      event.preventDefault();
      const sourceInputElements = document.getElementsByClassName(
        "add-product__source-js",
      );
      const lastSourceInputElement =
        sourceInputElements[sourceInputElements.length - 1];
      const clonedNode = lastSourceInputElement.cloneNode();
      clonedNode.value = "";
      lastSourceInputElement.parentNode.insertBefore(
        clonedNode,
        lastSourceInputElement.nextSibling,
      );
    });
}

document.addEventListener("DOMContentLoaded", () => {
  configureInputs();
  configureForm();
  configureAddSource();
});
