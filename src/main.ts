import { config } from "./config/config";
import { bootstrap } from "./bootstrap";

async function main() {
  const app = await bootstrap();

  await app.listen(config.port());
}

void main();
