import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { config } from "./config/config";
import { ApiModule } from "./api/api.module";
import { InterfaceModule } from "./interface/interface.module";

@Module({
  imports: [
    MongooseModule.forRoot(config.mongodbUri()),
    ApiModule,
    InterfaceModule,
  ],
})
export class AppModule {}
