import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";

@Schema()
export class Product {
  @Prop()
  _id: string;

  @Prop()
  carbonFootprint: number;

  @Prop()
  usage: number;

  @Prop()
  sources: string[];
}

export const ProductSchema = SchemaFactory.createForClass(Product);
