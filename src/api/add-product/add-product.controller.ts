import { BadRequestException, Body, Controller, Post } from "@nestjs/common";
import { AddProductRepository } from "./add-product.repository";
import {
  AddProductRequest,
  TotalUsage,
  Usage,
  UsagePerYear,
} from "./add-product-request";
import { Product } from "../../products/product.schema";

@Controller("/api/v1/products")
export class AddProductController {
  constructor(private readonly addProductRepository: AddProductRepository) {}

  @Post()
  async addProduct(@Body() addProductRequest: AddProductRequest) {
    const product = this.convertToProduct(addProductRequest);
    await this.addProductRepository.create(product);
  }

  convertToProduct(addProductRequest: AddProductRequest): Product {
    return {
      _id: addProductRequest.name,
      carbonFootprint: addProductRequest.carbonFootprint,
      usage: this.computeUsage(addProductRequest.usage),
      sources: addProductRequest.sources,
    };
  }

  computeUsage(usage: Usage): number {
    const isTotalUsage = (usage as TotalUsage).totalUsage !== undefined;
    if (isTotalUsage) {
      return (usage as TotalUsage).totalUsage;
    }

    const isUsagePerYear =
      (usage as UsagePerYear).yearlyUsage !== undefined &&
      (usage as UsagePerYear).lifespan !== undefined;
    if (isUsagePerYear) {
      return (
        (usage as UsagePerYear).yearlyUsage * (usage as UsagePerYear).lifespan
      );
    }

    throw new BadRequestException("Invalid usage");
  }
}
