import { Module } from "@nestjs/common";
import { AddProductController } from "./add-product.controller";
import { MongooseModule } from "@nestjs/mongoose";
import { Product, ProductSchema } from "../../products/product.schema";
import { AddProductRepository } from "./add-product.repository";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Product.name, schema: ProductSchema }]),
  ],
  providers: [AddProductRepository],
  controllers: [AddProductController],
})
export class AddProductModule {}
