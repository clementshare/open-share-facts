import { InjectModel } from "@nestjs/mongoose";
import { Product } from "../../products/product.schema";
import { Model } from "mongoose";

export class AddProductRepository {
  constructor(
    @InjectModel(Product.name) private productModel: Model<Product>,
  ) {}

  async create(product: Product): Promise<Product> {
    const newProduct = new this.productModel(product);
    return newProduct.save();
  }
}
