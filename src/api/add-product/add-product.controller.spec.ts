import { Test, TestingModule } from "@nestjs/testing";
import * as request from "supertest";
import { AddProductRepository } from "./add-product.repository";
import { AddProductController } from "./add-product.controller";
import { NestExpressApplication } from "@nestjs/platform-express";
import { getGlobalValidationPipe } from "../../bootstrap";

let app: NestExpressApplication;
let addProductRepository: AddProductRepository;

beforeAll(async () => {
  addProductRepository = <AddProductRepository>{};

  const module: TestingModule = await Test.createTestingModule({
    controllers: [AddProductController],
    providers: [AddProductRepository],
  })
    .overrideProvider(AddProductRepository)
    .useValue(addProductRepository)
    .compile();

  app = module.createNestApplication();
  app.useGlobalPipes(getGlobalValidationPipe());
  await app.init();
});

afterAll(async () => {
  await app.close();
});

test("201 with total usage", async () => {
  addProductRepository.create = jest.fn();

  await request(app.getHttpServer())
    .post("/api/v1/products")
    .send({
      name: "velo",
      carbonFootprint: 96000,
      usage: {
        totalUsage: 20,
      },
      sources: ["test"],
    })
    .expect(201);
});

test("201 with usage per year", async () => {
  addProductRepository.create = jest.fn();

  await request(app.getHttpServer())
    .post("/api/v1/products")
    .send({
      name: "velo",
      carbonFootprint: 96000,
      usage: {
        yearlyUsage: 2,
        lifespan: 20,
      },
      sources: ["test"],
    })
    .expect(201);
});

test("400 missing name", async () => {
  addProductRepository.create = jest.fn();

  await request(app.getHttpServer())
    .post("/api/v1/products")
    .send({
      carbonFootprint: 96000,
      usage: {
        totalUsage: 20,
      },
      sources: ["test"],
    })
    .expect(400);
});

test("400 missing carbon footprint", async () => {
  addProductRepository.create = jest.fn();

  await request(app.getHttpServer())
    .post("/api/v1/products")
    .send({
      name: "velo",
      usage: {
        totalUsage: 20,
      },
      sources: ["test"],
    })
    .expect(400);
});

test("400 missing usage", async () => {
  addProductRepository.create = jest.fn();

  await request(app.getHttpServer())
    .post("/api/v1/products")
    .send({
      name: "velo",
      carbonFootprint: 96000,
      sources: ["test"],
    })
    .expect(400);
});

test("400 empty usage", async () => {
  addProductRepository.create = jest.fn();

  await request(app.getHttpServer())
    .post("/api/v1/products")
    .send({
      name: "velo",
      carbonFootprint: 96000,
      usage: {},
      sources: ["test"],
    })
    .expect(400);
});

test("400 invalid usage", async () => {
  addProductRepository.create = jest.fn();

  await request(app.getHttpServer())
    .post("/api/v1/products")
    .send({
      name: "velo",
      carbonFootprint: 96000,
      usage: {
        yearlyUsage: 1,
      },
      sources: ["test"],
    })
    .expect(400);
});

test("400 missing sources", async () => {
  addProductRepository.create = jest.fn();

  await request(app.getHttpServer())
    .post("/api/v1/products")
    .send({
      name: "velo",
      carbonFootprint: 96000,
      usage: {
        totalUsage: 20,
      },
    })
    .expect(400);
});

test("400 empty sources", async () => {
  addProductRepository.create = jest.fn();

  await request(app.getHttpServer())
    .post("/api/v1/products")
    .send({
      name: "velo",
      carbonFootprint: 96000,
      usage: {
        totalUsage: 20,
      },
      sources: [],
    })
    .expect(400);
});
