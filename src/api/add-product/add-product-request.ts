import { ApiExtraModels, ApiProperty, getSchemaPath } from "@nestjs/swagger";
import {
  ArrayMinSize,
  IsArray,
  IsNotEmptyObject,
  IsNumber,
  IsString,
} from "class-validator";
import { Type } from "class-transformer";

export class UsagePerYear {
  @ApiProperty({
    description: "Utilisation annuelle du produit (en heures)",
  })
  readonly yearlyUsage: number;

  @ApiProperty({
    description: "Durée de vie du produit (en années)",
  })
  readonly lifespan: number;

  constructor(yearlyUsage: number, lifespan: number) {
    this.yearlyUsage = yearlyUsage;
    this.lifespan = lifespan;
  }
}

export class TotalUsage {
  @ApiProperty({
    description: "Durée totale d'utilisation du produit (en heures)",
  })
  @IsNumber()
  @Type(() => Number)
  readonly totalUsage: number;

  constructor(totalUsage: number) {
    this.totalUsage = totalUsage;
  }
}

export type Usage = TotalUsage | UsagePerYear;

@ApiExtraModels(TotalUsage, UsagePerYear)
export class AddProductRequest {
  @ApiProperty({
    description: "Typologie du produit",
  })
  @IsString()
  readonly name: string;

  @ApiProperty({
    description: "Facteur d'émission = bilan carbone (en gCO2)",
  })
  @IsNumber()
  @Type(() => Number)
  readonly carbonFootprint: number;

  @ApiProperty({
    description:
      "Facteur d'utilité, en utilisation totale ou en utilisation annuelle avec la durée de vie",
    oneOf: [
      { $ref: getSchemaPath(TotalUsage) },
      { $ref: getSchemaPath(UsagePerYear) },
    ],
  })
  @IsNotEmptyObject()
  readonly usage: Usage;

  @ApiProperty({
    description: "Sources des informations",
    isArray: true,
    type: String,
  })
  @IsArray()
  @ArrayMinSize(1)
  @IsString({ each: true })
  readonly sources: string[];

  constructor(
    name: string,
    carbonFootprint: number,
    usage: Usage,
    sources: string[],
  ) {
    this.name = name;
    this.carbonFootprint = carbonFootprint;
    this.usage = usage;
    this.sources = sources;
  }
}
