import { Module } from "@nestjs/common";
import { GetBorrowingSavingModule } from "./get-borrowing-saving/get-borrowing-saving.module";
import { AddProductModule } from "./add-product/add-product.module";

@Module({
  imports: [GetBorrowingSavingModule, AddProductModule],
})
export class ApiModule {}
