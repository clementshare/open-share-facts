import { Module } from "@nestjs/common";
import { GetBorrowingSavingController } from "./get-borrowing-saving.controller";
import { MongooseModule } from "@nestjs/mongoose";
import { Product, ProductSchema } from "../../products/product.schema";
import { GetBorrowingSavingRepository } from "./get-borrowing-saving.repository";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Product.name, schema: ProductSchema }]),
  ],
  providers: [GetBorrowingSavingRepository],
  controllers: [GetBorrowingSavingController],
})
export class GetBorrowingSavingModule {}
