import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { Product } from "../../products/product.schema";

class ProductResponse {
  @ApiProperty({
    description: "Typologie du produit",
  })
  readonly name: string;

  @ApiProperty({
    description: "Facteur d'émission = bilan carbone (en gCO2)",
  })
  readonly carbonFootprint: number;

  @ApiProperty({
    description:
      "Facteur d'utilité = durée d'utilisation totale du produit (en heures)",
  })
  readonly usage: number;

  @ApiProperty({
    description: "Sources des informations",
    isArray: true,
    type: String,
  })
  readonly sources: string[];

  constructor(product: Product) {
    this.name = product._id;
    this.carbonFootprint = product.carbonFootprint;
    this.usage = product.usage;
    this.sources = product.sources;
  }
}

export class BorrowingSavingResponse {
  @ApiProperty({
    description:
      "Economie de CO2 réalisée grâce à l'emprunt (en gCO2 par heure)",
  })
  readonly gCO2: number;

  @ApiPropertyOptional({
    description: "Durée de l'emprunt (en heures)",
  })
  readonly duration?: number;

  @ApiProperty()
  readonly product: ProductResponse;

  constructor(product: Product, duration?: number) {
    this.gCO2 = this.computeSaving(product, duration);
    this.duration = duration;
    this.product = new ProductResponse(product);
  }

  computeSaving(product: Product, duration?: number): number {
    const saving = product.carbonFootprint / product.usage;
    if (duration) {
      return saving * duration;
    } else {
      return saving;
    }
  }
}
