import { GetBorrowingSavingController } from "./get-borrowing-saving.controller";
import { GetBorrowingSavingRepository } from "./get-borrowing-saving.repository";
import { Test, TestingModule } from "@nestjs/testing";
import * as request from "supertest";
import {
  NestExpressApplication,
} from "@nestjs/platform-express";
import { getGlobalValidationPipe } from "../../bootstrap";

let app: NestExpressApplication;
let getBorrowingSavingRepository: GetBorrowingSavingRepository;

beforeAll(async () => {
  getBorrowingSavingRepository = <GetBorrowingSavingRepository>{};

  const module: TestingModule = await Test.createTestingModule({
    controllers: [GetBorrowingSavingController],
    providers: [GetBorrowingSavingRepository],
  })
    .overrideProvider(GetBorrowingSavingRepository)
    .useValue(getBorrowingSavingRepository)
    .compile();

  app = module.createNestApplication();
  app.useGlobalPipes(getGlobalValidationPipe());
  await app.init();
});

afterAll(async () => {
  await app.close();
});

test("200", async () => {
  getBorrowingSavingRepository.getProduct = jest.fn().mockResolvedValue({
    _id: "velo",
    carbonFootprint: 96000,
    usage: 20,
    sources: ["test"],
  });

  await request(app.getHttpServer())
    .get("/api/v1/borrowings/savings?product=velo")
    .expect(200)
    .expect({
      gCO2: 4800,
      product: {
        name: "velo",
        carbonFootprint: 96000,
        usage: 20,
        sources: ["test"],
      },
    });
});

test("200 with duration", async () => {
  getBorrowingSavingRepository.getProduct = jest.fn().mockResolvedValue({
    _id: "velo",
    carbonFootprint: 96000,
    usage: 20,
    sources: ["test"],
  });

  await request(app.getHttpServer())
    .get("/api/v1/borrowings/savings?product=velo&duration=3")
    .expect(200)
    .expect({
      gCO2: 14400,
      duration: 3,
      product: {
        name: "velo",
        carbonFootprint: 96000,
        usage: 20,
        sources: ["test"],
      },
    });
});

test("400 missing product", async () => {
  getBorrowingSavingRepository.getProduct = jest.fn().mockResolvedValue(null);

  await request(app.getHttpServer())
    .get("/api/v1/borrowings/savings")
    .expect(400);
});

test("400 negative duration", async () => {
  getBorrowingSavingRepository.getProduct = jest.fn().mockResolvedValue({
    _id: "velo",
    carbonFootprint: 96000,
    usage: 20,
    sources: ["test"],
  });

  await request(app.getHttpServer())
    .get("/api/v1/borrowings/savings?product=velo&duration=-3")
    .expect(400);
});

test("404 product not found", async () => {
  getBorrowingSavingRepository.getProduct = jest.fn().mockResolvedValue(null);

  await request(app.getHttpServer())
    .get("/api/v1/borrowings/savings?product=velo")
    .expect(404);
});
