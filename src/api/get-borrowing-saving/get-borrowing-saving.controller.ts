import { Controller, Get, NotFoundException, Query } from "@nestjs/common";
import { GetBorrowingSavingRepository } from "./get-borrowing-saving.repository";
import { BorrowingSavingResponse } from "./borrowin-saving-response";
import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import {
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  Min,
} from "class-validator";
import { Type } from "class-transformer";

class BorrowingSavingParams {
  @ApiProperty({
    description: "Typologie du produit",
  })
  @IsString()
  @IsNotEmpty()
  readonly product: string;

  @ApiPropertyOptional({
    description: "Durée de l'emprunt (en heures)",
  })
  @IsOptional()
  @IsNumber()
  @Type(() => Number)
  @Min(0)
  readonly duration?: number;

  constructor(product: string, duration?: number) {
    this.product = product;
    this.duration = duration;
  }
}

@Controller("/api/v1/borrowings/savings")
export class GetBorrowingSavingController {
  constructor(
    private getBorrowingSavingRepository: GetBorrowingSavingRepository,
  ) {}

  @Get()
  async getBorrowingSaving(
    @Query() { product, duration }: BorrowingSavingParams,
  ): Promise<BorrowingSavingResponse> {
    const foundProduct = await this.getBorrowingSavingRepository.getProduct(
      product,
    );
    if (!foundProduct) {
      throw new NotFoundException("Missing product");
    }

    return new BorrowingSavingResponse(foundProduct, duration);
  }
}
