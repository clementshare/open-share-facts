import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { Product } from "../../products/product.schema";

@Injectable()
export class GetBorrowingSavingRepository {
  constructor(
    @InjectModel(Product.name) private productModel: Model<Product>,
  ) {}

  async getProduct(product: string): Promise<Product | null> {
    return this.productModel.findOne({ _id: product });
  }
}
