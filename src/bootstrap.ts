import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { INestApplication, ValidationPipe } from "@nestjs/common";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";
import { join } from "path";

const configureSwagger = (app: INestApplication): void => {
  SwaggerModule.setup(
    "/",
    app,
    SwaggerModule.createDocument(
      app,
      new DocumentBuilder()
        .setTitle("Open Share Facts API")
        .setDescription("Requests on Open Share Facts database")
        .setVersion("1.0")
        .build(),
    ),
  );
};

export const getGlobalValidationPipe = (): ValidationPipe => {
  return new ValidationPipe({
    transform: true,
    whitelist: true,
    forbidUnknownValues: true,
  });
};

function configureFront(app) {
  app.useStaticAssets(join(__dirname, "..", "public"));
  app.setBaseViewsDir(join(__dirname, "..", "views"));
  app.setViewEngine("hbs");
}

export async function bootstrap(): Promise<INestApplication> {
  const app = await NestFactory.create(AppModule);

  configureSwagger(app);
  configureFront(app);

  app.useGlobalPipes(getGlobalValidationPipe());

  return app;
}
