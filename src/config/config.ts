import * as env from "env-var";

export class Config {
  mongodbUri(): string {
    return env
      .get("MONGODB_URI")
      .default("mongodb://127.0.0.1:27017/openShareFacts")
      .asString();
  }

  port(): number {
    return env.get("PORT").default(8080).asPortNumber();
  }
}

export const config = new Config();
