import { Module, Provider } from "@nestjs/common";
import { config, Config } from "./config";

const configProvider: Provider = {
  provide: Config,
  useValue: config,
};

@Module({
  providers: [configProvider],
  exports: [configProvider],
})
export class ConfigModule {}
