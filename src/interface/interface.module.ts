import { Module } from "@nestjs/common";
import { AddProductModule } from "./add-product/add-product.module";

@Module({
  imports: [
    AddProductModule,
  ],
})
export class InterfaceModule {}
