import { Controller, Get, Render } from "@nestjs/common";
import { ApiExcludeEndpoint } from "@nestjs/swagger";

@Controller("/interface")
export class AddProductController {
  @Get()
  @ApiExcludeEndpoint()
  @Render("index")
  root() {
    return {};
  }
}
