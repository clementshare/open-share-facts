import { Module } from "@nestjs/common";
import { AddProductController } from "./add-product.controller";

@Module({
  controllers: [AddProductController],
})
export class AddProductModule {}
